def test_postgresql_is_installed(host):
    postgresql = host.package("postgresql")
    print("""

    Test description:
    Is postgresql package installed 

    Action:
    Check if postgresql package is installed on the host by using python testinfra
    module. This test uses the apt package manager test if
    if the package is installed.

    Expected result:
    Return value should be "PASSED"

    Actual result:
    Is postgresql package installed: {}
    """.format(postgresql.is_installed))
    assert postgresql.is_installed

def test_postgresql_service_running_and_enabled(host):
    print("""

    Test description:
    Is postgresql service up and running

    Action:
    Check if postgresql service is running by using testinfra module.
    The test checks Systemd configuration if the specific module is started and
    enabled on boot.

    Expected result:
    Return value should be "PASSED"
    """)
    psql = host.service("postgresql")
    assert psql.is_running
    assert psql.is_enabled

def test_docker_container(host):
    cmd = host.run("docker run hello-world")
    print(cmd.stdout)
    print(cmd.succeeded)

def test_dns(host):
     google = host.addr("google.com")
     print(google.is_resolvable)